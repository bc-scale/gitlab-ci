
# - - Pluralith Network Module - -

# - -
# - - Network Resource Creation - -
# - -

# - - VPC - -
resource "aws_vpc" "PluralithVPC" {
    cidr_block = "10.0.0.0/16"

    tags = {
        Name = "PluralithVPC"
    }
}

# - - Public Subnets - -
resource "aws_subnet" "PluralithPublicNet1" {
    vpc_id = aws_vpc.PluralithVPC.id
    cidr_block = "10.0.1.0/24"
    availability_zone = "eu-central-1a"
    map_public_ip_on_launch = true

    tags = {
        Name = "PluralithPublicNet1"
    }
}

resource "aws_subnet" "PluralithPublicNet2" {
    vpc_id = aws_vpc.PluralithVPC.id
    cidr_block = "10.0.2.0/24"
    availability_zone = "eu-central-1b"
    map_public_ip_on_launch = true

    tags = {
        Name = "PluralithPublicNet2"
    }
}

# - - Private Subnets - -
resource "aws_subnet" "PluralithPrivateNet1" {
    vpc_id = aws_vpc.PluralithVPC.id
    cidr_block = "10.0.3.0/24"
    availability_zone = "eu-central-1a"
    map_public_ip_on_launch = false

    tags = {
        Name = "PluralithPrivateNet1"
    }
}

resource "aws_subnet" "PluralithPrivateNet2" {
    vpc_id = aws_vpc.PluralithVPC.id
    cidr_block = "10.0.4.0/24"
    availability_zone = "eu-central-1b"
    map_public_ip_on_launch = false

    tags = {
        Name = "PluralithPrivateNet2"
    }
}

# - - Internet Gateway - -
resource "aws_internet_gateway" "PluralithInternetGateway" {
    vpc_id = aws_vpc.PluralithVPC.id

    tags = {
        Name = "PluralithInternetGateway"
    }
}

# - - NAT IP Addresses - -
resource "aws_eip" "NAT1Eip" {
    vpc = "true"
    depends_on = [ aws_internet_gateway.PluralithInternetGateway ]
}

resource "aws_eip" "NAT2Eip" {
    vpc = "true"
    depends_on = [ aws_internet_gateway.PluralithInternetGateway ]
}

# - - NAT Gateways - -
resource "aws_nat_gateway" "PluralithNAT1" {
    allocation_id = aws_eip.NAT1Eip.id
    subnet_id = aws_subnet.PluralithPublicNet1.id

    tags = {
        Name = "PluralithNAT1"
    }
}

resource "aws_nat_gateway" "PluralithNAT2" {
    allocation_id = aws_eip.NAT2Eip.id
    subnet_id = aws_subnet.PluralithPublicNet2.id

    tags = {
        Name = "PluralithNAT2"
    }
}


# - -
# - - Network Routing - -
# - -

# - - Public Route Table - -
resource "aws_route_table" "PluralithPublicRouteTable" {
    vpc_id = aws_vpc.PluralithVPC.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.PluralithInternetGateway.id
    }

    tags = {
        Name = "PluralithPublicRouteTable"
    }
}

# - - Public Route Table Associations - -
resource "aws_route_table_association" "PluralithPublicRouteAssoc1" {
    subnet_id = aws_subnet.PluralithPublicNet1.id
    route_table_id = aws_route_table.PluralithPublicRouteTable.id
}

resource "aws_route_table_association" "PluralithPublicRouteAssoc2" {
    subnet_id = aws_subnet.PluralithPublicNet2.id
    route_table_id = aws_route_table.PluralithPublicRouteTable.id
}


# - - Private Route Table 1 - -
resource "aws_route_table" "PluralithPrivateRouteTable1" {
    vpc_id = aws_vpc.PluralithVPC.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_nat_gateway.PluralithNAT1.id
    }

    tags = {
        Name = "PluralithPrivateRouteTable1"
    }
}

resource "aws_route_table_association" "PluralithPrivateRouteAssoc1" {
    subnet_id = aws_subnet.PluralithPrivateNet1.id
    route_table_id = aws_route_table.PluralithPrivateRouteTable1.id
}


# - - Private Route Table 2 - -
resource "aws_route_table" "PluralithPrivateRouteTable2" {
    vpc_id = aws_vpc.PluralithVPC.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_nat_gateway.PluralithNAT2.id
    }

    tags = {
        Name = "PluralithPrivateRouteTable1"
    }
}

resource "aws_route_table_association" "PluralithPrivateRouteAssoc2" {
    subnet_id = aws_subnet.PluralithPrivateNet2.id
    route_table_id = aws_route_table.PluralithPrivateRouteTable2.id
}



# - -
# - - Outputs - -
# - -

output "PluralithVPC" {
    value = aws_vpc.PluralithVPC.id
}

output "PluralithPrivateSubnets" {
    value = [ aws_subnet.PluralithPrivateNet1.id, aws_subnet.PluralithPrivateNet2.id ]
}

output "PluralithPublicSubnets" {
    value = [ aws_subnet.PluralithPublicNet1.id, aws_subnet.PluralithPublicNet2.id ]
}
